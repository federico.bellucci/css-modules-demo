import './index.css'
import * as styles from './app.module.css'

let element = `
  <div>
    <h1>CSS Modules demo</h1>
    <div class="${styles.element}">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur laudantium recusandae itaque libero velit minus ex reiciendis veniam. Eligendi modi sint delectus beatae nemo provident ratione maiores, voluptatibus a tempore!</p>
    </div>
  </div>
`

document.write(element);
